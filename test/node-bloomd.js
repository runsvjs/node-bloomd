'use strict';

require('dotenv').config();
const assert = require('assert');
const service = require('..');

describe('node-bloomd service wrapper', function(){
	describe('start/stop', function(){
		before(function(done){
			const self = this;
			self.service = service({
				debug: true, 
				host: process.env.BLOOMD_HOST, 
				port:process.env.BLOOMD_PORT
			});
			self.service.start(null, function(err){
				self.start = true;
				return done(err);
			});
		});
		before(function(done){
			const self = this;
			self.service.stop(function(err){
				self.stop = true;
				return done(err);
			});
		});
		it('should start', function(){
			assert(this.start);
		});
		it('should stop', function(){
			assert(this.stop);
		});
	});
	describe('When unavailable', function(){
		it('start should fail', function(done){
			const self = this;
			self.service = service({
				debug: true, 
				host: ';)', 
				port:1,
				maxConnectionAttempts: 1
			});
			self.service.start(null, function(err){
				if(err && err.message === 'unavailable'){
					return done();
				}
				assert.fail('did not fail');
			});
		});
	});
});
