'use strict';

const bloomd = require('bloomd');

const defaults = {
	name: 'bloomd'
};

function create(options){
	let client;
	const {name, 
		host, 
		port, 
		debug, 
		reconnectDelay, 
		maxConnectionAttempts} = {...defaults, ...options};
	return {
		name,
		start(_, callback){
			if(client){
				setImmediate(callback, new Error('already started'));
			}

			client = bloomd.createClient({host, port, debug, reconnectDelay, maxConnectionAttempts});

			function unavailable(){
				return done(new Error('unavailable'));
			}
			function done(err){
				if(done.fired){
					return;
				}
				done.fired = true;
				
				client.removeListener('connected', done);
				client.removeListener('unavailable', unavailable);
				// callback err, if any
				return callback(err);
			}
			client.once('connected', done);
			// will only happen if maxConnectionAttempts is set. Default is no limit (0).
			client.once('unavailable', unavailable);
		},
		stop(callback){
			if(!client){
				setImmediate(callback);
			}
			client.once('disconnected', callback);
			// dispose will terminate the stream
			client.dispose();
		},
		getClient: () => client

	};
}
module.exports = create;

