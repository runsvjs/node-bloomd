# node-bloomd

A [node-bloomd](https://github.com/Medium/node-bloomd) [runsv](https://gitlab.com/runsvjs/runsv) service wrapper.

[![pipeline status](https://gitlab.com/runsvjs/node-bloomd/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/node-bloomd/commits/master)
[![coverage report](https://gitlab.com/runsvjs/node-bloomd/badges/master/coverage.svg)](https://gitlab.com/runsvjs/node-bloomd/commits/master)
